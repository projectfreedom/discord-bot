# How to run this
Have node.js installed.<br>
Configure your bot in Discord.<br>
Create config.json locally and populate fields (example below)<br>
Run `node index.js`.<br>
Use the bot!

# How the config.json looks
```JSON
{
    "botToken": "the token of the bot",
    "ownerId": "the ID of the owner of the bot",
    "commandPrefix": "!"
}
```