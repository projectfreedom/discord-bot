const discord = require('discord.js')
const config = require('./config.json')
const commands = require('./commands.json')

const client = new discord.Client();

var lastChannelId;

client.on('ready', function() {
    console.log('Bot is ready! Logged in as "' + client.user.tag + '"');
});

client.on('message', function(message) {
    lastChannelId = message.channel.id;
    try {
        if (!message.content.startsWith(config.commandPrefix) || message.author.bot) return;

        if (message.content.startsWith(config.commandPrefix + 'help')) {
            commandHelp(message)
            return;
        }
        
        if (message.content.startsWith(config.commandPrefix + 'owner')) {
            commandOwner(message)
            return;
        }

        if (message.content.startsWith(config.commandPrefix + 'ask')) {
            commandAsk(message)
            return;
        }
    
        message.channel.send('Clearly you don\'t know how to use my commands. Let me help you out...');
        commandHelp(message);
    }
    catch (err) {
        var channel = client.channels.get(lastChannelId);
        channel.send('Ouch, that hurt. My code must not be very good.');
        channel.send(err.stack)
    }
});

function commandHelp(message) {
    message.channel.send("Here's my full list of commands:")
    commands.forEach(function(element) {
        message.channel.send(config.commandPrefix + element.command + ' -> ' + element.description);
    }, this);
}

function commandOwner(message) {
    message.channel.send('Your ID is "' + message.author.id + '" and my owner is "' + config.ownerId + '"');
    if (message.author.id === config.ownerId) {
        message.channel.send('Ah, ' + message.author.username + ' you are my owner!');
    }
    else {
        message.channel.send('Who do you think you are, ' + message.author.username + "? You're not allowed to do this.")
    }
}

function commandAsk(message) {
    var answers = ['no', 'try asking me again', 'yes', 'most definitely', 'unlikely but possible', 'very likely', '100% yes', 'I can\'t answer this']

    var question = message.content.substring(5, message.content.length)
    message.channel.send(message.author.username + ', you asked me _' + question + '_');
    message.channel.send('I pondered this question and my response is _' + answers[Math.floor(Math.random() * answers.length)] + '_');
}

client.login(config.botToken);